package com.awesome_fusion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tv =  findViewById(R.id.displayHeading);

        updateHeading();

    }


    /**
     * Update phones heading with a given intervall. The heading is refered to as the rotation
     * around z-axis with respect to north. I would like the heading to be a compass heading.
    *
     **/
    private void updateHeading() {

        tv.setText("Heading here");
    }

}
